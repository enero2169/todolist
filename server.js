const express = require("express");
const path = require("path");
const taskArray = require("./tasks.json");
const {users} = require("./models");

const app = express();
const PORT = 8000;

//Configuración EJS
//1. Definiendo en donde se ubicará el directorio views
app.set("views", path.join(__dirname, "views"));
//2. Definiendo el motor que usaremos
app.set("view engine", "ejs");

//Middleware incorporado (built-in)
//express.static nos servirá para poder servir archivos de forma estatica
app.use(express.static(path.join(__dirname, "public")));
app.use(express.urlencoded({extended: true})); //Permite procesar los datos enviados por el cliente a través de x-www-form-urlencoded
app.use(express.json()); //Permite procesar los datos enviados por el cliente a través de application/json

//Middleware de aplicacion trabaja con los métodos HTTP (GET, POST, PUT, DELETE)
//Van manejar los objetos request, response y una función llamada next
app.get("/", (request, response) => {
    response.render("pages/home", {
        title: "Inicio",
        message: "Hola mundo con EJS",
    });
});

app.get("/tareas", (request, response) => {
    response.render("pages/tasks", {
        title: "Tareas",
        message: "Lista de tareas",
        items: taskArray,
    });
});

app.get("/registro", (request, response) => {
  response.render("pages/register");
});

app.post("/registro", async (request, response, next) => {
  let {firstname, lastname, email, password} = request.body;
  try{
    await users.create({firstname, lastname, email, password});
    response.redirect("/registro");
  }catch(error){
    next(error);
  }
});

app.use((request, response) => {
  let pathNotFound = path.join(__dirname, "public", "404.html");
  response.status(404).sendFile(pathNotFound);
});

//Middleware para el manejo de errores
app.use((error, request, response, next) => {
  const errors = require("./utils/errorMessages");
  response.status(404).send(errors[error.name]);
});

app.listen(PORT, () => {
  console.log(`Servidor escuchando sobre el puerto ${PORT}`);
});
