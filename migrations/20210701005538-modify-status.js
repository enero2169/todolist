'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('statuses', 'title', {
      type: Sequelize.STRING,
      allowNull: false
    });
  }
};